"use strict"
const btnTheme = document.querySelector(".btn-theme");
const link = document.createElement('link');
link.setAttribute('rel','stylesheet');
link.setAttribute('href','./css/greentheme.css');
if(window.localStorage.getItem('theme')==='green'){
    document.head.append(link);
    document.querySelector("#orange").classList.remove('logo-hover');
    document.querySelector("#green").classList.add('logo-hover');
}
btnTheme.addEventListener("click", ()=>{
    if(localStorage.getItem('theme') === 'green'){
        link.remove();
        window.localStorage.setItem('theme', 'orange');
        document.querySelector("#green").classList.remove('logo-hover');
        document.querySelector("#orange").classList.add('logo-hover');
    }else{
        document.head.append(link);
        window.localStorage.setItem('theme', 'green');
        document.querySelector("#orange").classList.remove('logo-hover');
        document.querySelector("#green").classList.add('logo-hover');
    }
});